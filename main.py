import tkinter as tk
import socket
import threading
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import PBKDF2

PASSWORD = "supersecretpassword"
KEY_SIZE = 32
BLOCK_SIZE = 16
MESSAGE_ROTATE_COUNT = 5

IP = 'localhost'
PORT = 12345

class SecureMessagingApp:
    isClient = False

    def __init__(self, root):
        self.root = root
        self.root.title("Secure Messaging App")
        
        self.message_frame = tk.Frame(self.root)
        self.message_frame.pack(pady=10)
        
        self.message_label = tk.Label(self.message_frame, text="Messages:")
        self.message_label.grid(row=0, column=0, padx=10)
        
        self.message_text = tk.Text(self.message_frame, width=60, height=20) 
        self.message_text.grid(row=1, column=0, padx=10)
        
        self.input_frame = tk.Frame(self.root)
        self.input_frame.pack(pady=10)
        
        self.input_label = tk.Label(self.input_frame, text="Enter message:")
        self.input_label.grid(row=0, column=0, padx=10)
        
        self.input_entry = tk.Entry(self.input_frame, width=50)  
        self.input_entry.grid(row=0, column=1, padx=10)
        self.input_entry.bind("<Return>", self.send_message) # pressing enter sends message too. for convinience
        
        self.send_button = tk.Button(self.input_frame, text="Send", command=self.send_message)
        self.send_button.grid(row=0, column=2, padx=10)
        
        self.connection_status_label = tk.Label(self.root, text="Connection status: Not connected", fg="red")
        self.connection_status_label.pack(pady=5)
        
        self.client_socket = None
        self.server_socket = None
        self.is_server = False
        self.running = True # this is for trying to terminate the program properly after closing the window. but rn i cant get it to work properly :)))
        self.message_count = 0
        self.current_key = self.generate_key(PASSWORD)
        

        if self.is_server_setup():
            self.isClient = True
        
        if self.isClient:
            self.connect_to_server()
        else:
            self.setup_server()
        
        self.root.protocol("WM_DELETE_WINDOW", self.on_close)
        
    def is_server_setup(self):
        # Attempt to create a socket and bind it to the IP and port
        try:
            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server_socket.bind((IP, PORT))
            self.server_socket.close()  # Close the socket after binding
            return False  # Binding successful, server is not running
        except OSError as e:
            return True  # Binding failed, server is already running
            
    def setup_server(self):
        try:
            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server_socket.bind((IP, PORT))
            self.server_socket.listen(1)
            self.is_server = True
            self.receive_thread = threading.Thread(target=self.receive_messages)
            self.receive_thread.start()
            self.connection_status_label.config(text="Connection status: Waiting for client", fg="orange")
        except Exception as e:
            self.message_text.insert(tk.END, f"Error setting up server: {str(e)}\n")
    
    def connect_to_server(self):
        try:
            self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client_socket.connect((IP, PORT))
            self.receive_thread = threading.Thread(target=self.receive_messages)
            self.receive_thread.start()
            self.connection_status_label.config(text="Connection status: Connected to server", fg="green")
        except Exception as e:
            self.message_text.insert(tk.END, f"Error connecting to server: {str(e)}\n")
            
    # just some clean up so that the sockets are closed properly
    # also makes sure the program properly terminates after closing window
    def on_close(self):
        self.running = False
        if self.client_socket:
            self.client_socket.close()
        if self.server_socket:
            self.server_socket.close()
        if self.is_server and self.receive_thread.is_alive():
            self.receive_thread.join(timeout=1) 
        
        self.root.destroy()  

    def send_message(self, event=None):
        message = self.input_entry.get()
        if message:
            try:
                self.message_count += 1
                encrypted_message = self.encrypt_message(message)
                display_text = f"You (Plaintext): {message}\nYou (Encrypted): {encrypted_message}\n"
                self.message_text.insert(tk.END, display_text)
                if self.client_socket or self.is_server:
                    self.client_socket.send(encrypted_message.encode())
                    self.input_entry.delete(0, tk.END)
                    
                if self.message_count >= MESSAGE_ROTATE_COUNT:
                    self.current_key = self.generate_key(PASSWORD)  # regens key after 5 messages
                    self.message_count = 0 
            except Exception as e:
                self.message_text.insert(tk.END, f"Error sending message: {str(e)}\n")
    
    def receive_messages(self):
        while self.running:
            try:
                if self.client_socket is None and self.is_server:
                    client_socket, _ = self.server_socket.accept()
                    self.client_socket = client_socket
                    self.connection_status_label.config(text="Connection status: Connected to client", fg="green")
                encrypted_message = self.client_socket.recv(1024).decode()
                if encrypted_message:
                    decrypted_message = self.decrypt_message(encrypted_message)
                    display_text = f"Client (Encrypted): {encrypted_message}\nClient (Plaintext): {decrypted_message}\n"
                    self.message_text.insert(tk.END, display_text)
            except Exception as e:
                if self.client_socket:
                    self.connection_status_label.config(text="Connection status: Not connected", fg="red")
                self.message_text.insert(tk.END, f"Error receiving message: {str(e)}\n")
                break

    # used CBC instead because it uses an initialization vector
    def encrypt_message(self, message):
        key = self.current_key
        initialization_vector = get_random_bytes(BLOCK_SIZE) # adds randomness to the encryption
        padded_message = self.pad_message(message)
        cipher = AES.new(key, AES.MODE_CBC, initialization_vector)
        encrypted_message = cipher.encrypt(padded_message)
        
        final_message = initialization_vector + encrypted_message
        return final_message.hex() # return the vector and message
    
    def decrypt_message(self, encrypted_message):
        key = self.current_key
        initialization_vector = bytes.fromhex(encrypted_message[:BLOCK_SIZE * 2]) 
        encrypted_message = bytes.fromhex(encrypted_message[BLOCK_SIZE * 2:])
        cipher = AES.new(key, AES.MODE_CBC, initialization_vector)
        decrypted_message = cipher.decrypt(encrypted_message)
        unpadded_message = self.unpad_message(decrypted_message)
        return unpadded_message.decode()
    
    def generate_key(self, password):
        salt = b'salt'
        key = PBKDF2(password, salt, dkLen=KEY_SIZE, count=1000000)
        return key
    
    def pad_message(self, message):
        padding_length = BLOCK_SIZE - len(message) % BLOCK_SIZE
        padded_message = message + padding_length * chr(padding_length)
        return padded_message.encode()
    
    def unpad_message(self, padded_message):
        padding_length = padded_message[-1]
        unpadded_message = padded_message[:-padding_length]
        return unpadded_message

def main():
    root = tk.Tk()
    app = SecureMessagingApp(root)
    root.mainloop()

if __name__ == "__main__":
    main()