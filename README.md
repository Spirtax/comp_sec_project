# Secure Instant Point-to-Point (P2P) Messaging System

## Overview

This project is a secure instant messaging tool for Alice and Bob, enabling them to send encrypted messages over the internet. The system utilizes a shared password to generate a key for encryption and decryption, ensuring secure communication.

## Features

* Secure key generation using PBKDF2 and a salt value
* AES encryption with a 56-bit key in CBC mode
* Random initialization vector (IV) for each message encryption
* Periodic key rotation every 5 messages
* Graphical user interface (GUI) for easy interaction
* Socket programming for secure communication between Alice and Bob

## Usage

1. Run the program on both Alice's and Bob's machines.
2. If Bob is running the server, Alice will connect to Bob's IP address and port.
3. Enter messages in the input field and press Send or Enter to encrypt and send the message.
4. Received messages will be displayed in the message text area.

## Security

* The system uses a shared password to generate a key for encryption and decryption.
* PBKDF2 and a salt value prevent brute-force attacks on the password.
* AES encryption with a 56-bit key in CBC mode ensures secure communication.
* Random IVs prevent identical ciphertext for identical messages.
* Periodic key rotation every 5 messages prevents long-term key exposure.

## Note

* This system is for demonstration purposes only and should not be used for secure communication in production.
* The password and key generation mechanisms are simplified for demonstration purposes and should be strengthened in a production environment.